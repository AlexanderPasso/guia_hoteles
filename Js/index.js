
		$(function(){
			
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 2000
			});

			$('#contacto').on('show.bs.modal',function(e){
				console.log("El modal se está mostrando");
				$('#registrarBtn').removeClass('btn-outline-success');
				$('#registrarBtn').addClass('btn-primary');
				
				$('#registrarBtn').prop('disabled',true);
			});
			$('#contacto').on('shown.bs.modal',function(e){
				console.log("El modal se mostró");
			});
			$('#contacto').on('hide.bs.modal',function(e){
				console.log("El modal se oculta");
			});
			$('#contacto').on('hidden.bs.modal',function(e){
				console.log("El modal se ocultó");
				
				$('#registrarBtn').prop('disabled',false);
				$('#registrarBtn').addClass('btn-outline-success');
				
			});
		});
